module HeapBase.Compiler (compile) where

import HeapBase.Parser
import HeapBase.VM
import HeapBase.CommonData

isTail :: VMInsn -> Bool
isTail insn = insn == Return

compileLookup :: String -> CTEnv -> Int
compileLookup varname env = let go [] ind = error "compileLookup: unbounded variable"
                                go (v:env) ind = if v == varname then ind else go env (1 + ind)
                            in go env 0

extend :: [VarName] -> CTEnv -> CTEnv
extend vars env = (reverse vars) ++ env

-- (define (compile exp next) ...)
compile :: LispVal -> CTEnv -> VMInsn -> VMInsn
compile (Atom varname) env next = Refer (compileLookup varname env) next
compile (List ((Atom "quote"):[obj])) env next = Constant obj next
compile (List ((Atom "lambda"):(List vars):body)) env next = let compileBody [] env = Return
                                                                 compileBody (next:body) env = let c = compileBody body env
                                                                                               in compile next env c
                                                                 unwrapAtom (Atom v) = v
                                                                 unwrapAtom _ = error "unwrapAtom: not Atom."
                                                                 varnames = map unwrapAtom vars
                                                             in Close (compileBody body $ extend varnames env) next
compile (List ((Atom "lambda"):_:body)) env next = error "compile: (lambda (x y z) <- this must be a list."
compile (List ((Atom "if"):testx:thenx:[elsex])) env next = let thenc = compile thenx env next
                                                                elsec = compile elsex env next
                                                            in compile testx env (Test thenc elsec)
compile (List ((Atom "set!"):(Atom var):[x])) env next = compile x env (Assign (compileLookup var env) next)
compile (List ((Atom "call/cc"):[x])) env next = let c = Conti $ Argument $ compile x env Apply
                                                 in if isTail next
                                                    then c
                                                    else Frame next c
compile (List (fn:args)) env next = let go [] env c = if isTail next then c else Frame next c
                                        go (a:args) env c = go args env $ compile a env $ Argument c -- args evaluated from right to left. if (f a b) then b -> a.
                                    in go args env (compile fn env Apply)
compile val env next = Constant val next
