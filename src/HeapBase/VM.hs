module HeapBase.VM where

import HeapBase.Parser
import Control.Monad.State
import HeapBase.CommonData
import Data.Vector as DV (modify, replicate, (!), Vector(..))
import Data.Vector.Mutable as DVM (write)

vmExtend :: VMEnv -> Rib -> State VMStore VMEnv
vmExtend e [] = return e
vmExtend e (val:vals) = do
  addr <- allocVMStore val
  vmExtend (addr:e) vals

lookupEnv :: Int -> VMEnv -> Int -- ind -> env -> addr
lookupEnv ind e = e !! ind

putArg :: LispVal -> Rib -> Rib
putArg v r = v:r

type VMStore = (Int, Vector LispVal) -- (head, store)

headAddr :: VMStore -> Int
headAddr = fst

store :: VMStore -> Vector LispVal
store = snd

getFromVMStore :: VMStore -> Int -> LispVal
getFromVMStore vmStore addr = (store vmStore) ! addr

assignVMStore :: Int -> LispVal -> State VMStore LispVal
assignVMStore addr val = do
  vmStore <- get
  put $ (headAddr vmStore, DV.modify (\store -> DVM.write store addr val) (store vmStore))
  return val

allocVMStore :: LispVal -> State VMStore Int
allocVMStore v = do
  vmStore <- get
  let addr = headAddr vmStore
  assignVMStore addr v
  vmStore <- get
  put (1 + addr, store vmStore)
  return $ addr

vmMemSize :: Int
vmMemSize = 10000

vmStore :: VMStore
vmStore = (0,DV.replicate vmMemSize Undefined)

makeContinuation :: VMStack -> LispVal
makeContinuation s = Closure (Nuate s 0) emptyEnv

getVal :: Int -> VMEnv -> State VMStore LispVal
getVal ind e = do
  store <- get
  let address = lookupEnv ind e :: Int
  return $ getFromVMStore store address

-- (define (vm a x e r s) ...)
 -- x is next instruction
vm :: LispVal -> VMInsn -> VMEnv -> Rib -> VMStack -> State VMStore LispVal
vm a Halt e r s = return a
vm a (Refer ind x) e r s = do
  val <- getVal ind e
  vm val x e r s
vm a (Constant obj x) e r s = vm obj x e r s
vm a (Close body x) e r s = vm (Closure body e) x e r s
vm a (Test thenx elsex) e r s = case a of
                                  Bool False -> vm a elsex e r s
                                  otherwise -> vm a thenx e r s
vm a (Assign ind x) e r s = do
  store <- get
  let address = lookupEnv ind e :: Int
  assignVMStore address a
  vm a x e r s
vm a (Conti x) e r s = vm (makeContinuation s) x e r s
vm a (Nuate s ind) e r _ = do -- NOTE: passed stack is not used
  val <- getVal ind e
  vm val Return e r s
vm a (Frame ret x) e r s = vm a x e emptyRib $ CallFrame ret e r s
vm a (Argument x) e r s = vm a x e (putArg a r) s
vm a Apply e r s = case a of
                     Closure body e -> do
                       ne <- vmExtend e r
                       vm a body ne emptyRib s
                     _ -> error $ "vm: invalid application: a is not a closure a=" Prelude.++ show a
vm a Return e r s = case s of
                      EmptyStack -> error "vm: stack is empty, but you tried to return"
                      CallFrame x e r s -> vm a x e r s

startVM :: LispVal -> VMInsn -> VMEnv -> Rib -> VMStack -> LispVal
startVM a x e r s = fst $ runState (vm a x e r s) vmStore
