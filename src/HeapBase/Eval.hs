module HeapBase.Eval where

import HeapBase.Parser
import HeapBase.VM
import HeapBase.Compiler
import HeapBase.CommonData

evaluate :: String -> LispVal
evaluate s = case myParse s of
               Right res -> startVM Undefined (compile res emptyCTEnv Halt) emptyEnv emptyRib emptyVMStack
               Left _ -> error $ "invalid scheme exprssion: " ++ s
