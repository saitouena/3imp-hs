module HeapBase.CommonData where

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool
             | Closure VMInsn VMEnv -- from here it appears after evaluation. no longer vars are needed.
             | Undefined
 deriving (Show, Eq)

type VMEnv = [Int]

emptyEnv :: VMEnv
emptyEnv = []

type CTEnv = [VarName] -- Compile Time Environment

emptyCTEnv :: CTEnv
emptyCTEnv = []

type VarName = String


data VMInsn = Halt -- (halt)
            | Refer Int VMInsn --  (refer access x)
            | Constant LispVal VMInsn -- (constant var x)
            | Close VMInsn VMInsn -- (close body x) no longer vars are needed.
            | Test VMInsn VMInsn -- (test then else)
            | Assign Int VMInsn -- (assign access x)
            | Conti VMInsn -- (conti x)
            | Nuate VMStack Int -- (nuate s var)
            | Frame VMInsn VMInsn -- (frame ret x)
            | Argument VMInsn -- (argument x)
            | Apply -- (apply)
            | Return -- (return)
            deriving (Eq,Show)

-- instance Show VMInsn where
--   show Halt = "Halt\n"
--   show (Refer var insn) = "Refer " ++ var ++ "\n" ++ show insn
--   show (Constant val insn) = "Constant " ++ show val ++ "\n" ++ show insn
--   show (Close vars body next) = "Close " ++ show vars ++ "\n"++"body: -----\n" ++ show insn ++ "-----\n"++ show next
--   show (Test thenx elsex) = "Test \n" ++ 

data VMStack = EmptyStack
             | CallFrame VMInsn VMEnv Rib VMStack -- vmInsn = return address (executed after return)
  deriving (Show, Eq)

emptyVMStack :: VMStack
emptyVMStack = EmptyStack

-- | TODO: current just a stub.
type Rib = [LispVal]

emptyRib :: Rib
emptyRib = []
