module StackBase.Compiler where

import StackBase.CommonData

type CTEnv = [[String]]

compileLookup :: String -> CTEnv -> Maybe (Int,Int)
compileLookup varname env = let go1 n [] = Nothing
                                go1 n (frame:env) = case go2 0 frame of
                                                      Just m -> return (n,m)
                                                      Nothing -> go1 (1 + n) env
                                go2 m [] = Nothing
                                go2 m (var:vars) = if var == varname
                                                   then return m
                                                   else go2 (1 + m) vars
                            in go1 0 env

extend :: [String] -> CTEnv -> CTEnv
extend vars env = vars:env

compile :: LispVal -> CTEnv -> VMInsn -> VMInsn
compile (Atom varname) env next = case compileLookup varname env of
                                    Just (n,m) -> Refer n m next
                                    Nothing -> error $ "compile: unbounded variable " ++ varname
compile (List ((Atom "quote"):[obj])) env next = Constant obj next
compile (List ((Atom "lambda"):(List vars):body)) env next = let compileBody [] env = Return (1 + (length vars))
                                                                 compileBody (stmt:body) env = let c = compileBody body env
                                                                                               in compile stmt env c
                                                                 f (Atom x) = x
                                                                 f _ = error "compile: lambda vars not list of atom."
                                                             in Close (compileBody body (extend (map f vars) env)) next
compile (List ((Atom "lambda"):_:body)) env next = error "compile: lambda vars must be list"
compile (List ((Atom "if"):testx:thenx:[elsex])) env next = let thenc = compile thenx env next
                                                                elsec = compile elsex env next
                                                            in compile testx env (Test thenc elsec)
compile (List ((Atom "set!"):(Atom var):[x])) env next = case compileLookup var env of
                                                           Just (n,m) -> compile x env $ Assign n m next
                                                           Nothing -> error "compile: set! var is not bounded"
compile (List (fn:args)) env next = let compileArgs [] c = Frame next c -- Frame ... (compile argn) ... Argument ... (compile arg1) ... Argument ... (compile fn) Apply
                                        compileArgs (a:args) c = compileArgs args $ compile a env $ Argument c
                                    in compileArgs args $ compile fn env Apply
compile val env next = Constant val next
