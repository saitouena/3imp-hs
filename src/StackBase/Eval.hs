module StackBase.Eval where

import StackBase.CommonData
import StackBase.Parser
import StackBase.Compiler
import StackBase.VM

evaluate :: String -> LispVal
evaluate s = case myParse s of
               Right res -> startVM Undefined (compile res [] Halt) 0 0
               otherwise -> error "evaluate: parse error"
