module StackBase.VM where

import StackBase.CommonData
import Control.Monad.State
import Data.Vector as DV
import Data.Vector.Mutable as DVM (write)

-- stack data definition and operation
type VMStack = Vector LispVal

stackSize :: Int
stackSize = 1000

vmStack :: Vector LispVal
vmStack = DV.replicate stackSize Undefined

push :: LispVal -> Int -> VMStack -> VMStack
push v sp stack = DV.modify (\stack -> DVM.write stack sp v) stack

index :: Int -> Int -> VMStack -> LispVal
index sp offset stack = stack ! (sp - offset - 1)

indexSet :: Int -> Int -> LispVal -> VMStack -> VMStack
indexSet sp offset v stack = DV.modify (\stack -> DVM.write stack (sp - offset - 1) v) stack

findLink :: Int -> Int -> VMStack -> Int
findLink 0 e stack = e
findLink n e stack = case index e (-1) stack of
                       Link e -> findLink (n - 1) e stack
                       otherwise -> error "findLink: e doesn't points link"

vm :: LispVal -> VMInsn -> StaticLink -> DynamicLink -> State VMStack LispVal
vm a Halt e s = return a
vm a (Refer n m x) e s = do
  stack <- get
  let val = index (findLink n e stack) m stack
  vm val x e s
vm a (Constant v x) e s = vm v x e s
vm a (Close body next) e s = vm (Functional body e) next e s
vm a (Test thenx elsex) e s = case a of
                                Bool False -> vm a elsex e s
                                otherwise -> vm a thenx e s
vm a (Assign n m x) e s = do
  oldStack <- get
  let newStack = indexSet (findLink n e oldStack) m a oldStack
  put newStack
  vm a x e s
vm a (Frame ret x) e s = do
  stack <- get 
  put $ push (Link e) s stack
  stack <- get
  put $ push (Addr ret) (s+1) stack
  vm a x e (s + 2)
vm a (Argument x) e s = do
  stack <- get
  put $ push a s stack
  vm a x e (s+1)
vm a@(Functional body link) Apply e s = do -- link = static link???
  stack <- get
  put $ push (Link link) s stack
  vm a body s (s+1)
vm _ Apply e s = error "vm: invalid application. a is not functional"
vm a (Return n) e s = do
  let s = s-n
  stack <- get
  case (index s 0 stack,index s 1 stack,index s 2 stack) of
    (Addr x, Link e, Link s) -> vm a x e s
    otherwise -> error "vm: invalid return."

startVM :: LispVal -> VMInsn -> StaticLink -> DynamicLink -> LispVal
startVM a x e s = fst $ runState (vm a x e s) vmStack
