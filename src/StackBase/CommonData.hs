module StackBase.CommonData where

type StaticLink = Int

type DynamicLink = Int

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool
             | Functional VMInsn StaticLink -- functional object doesn't have infinite extent.
             | Undefined
             | Link Int -- used for dynamic link, static link
             | Addr VMInsn
             deriving (Show, Eq)

data VMInsn = Halt
            | Refer Int Int VMInsn
            | Constant LispVal VMInsn
            | Close VMInsn VMInsn -- (close body next)
            | Test VMInsn VMInsn
            | Assign Int Int VMInsn
            | Frame VMInsn VMInsn
            | Argument VMInsn
            | Apply
            | Return Int
            deriving (Show, Eq)
