module Main where

import HeapBase.Parser (myParse)

main :: IO ()
main =
  case myParse "(+ 1 2 3)" of
    Right s -> putStrLn $ show s
    Left _  -> putStrLn "error"
