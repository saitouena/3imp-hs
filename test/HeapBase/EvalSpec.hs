module HeapBase.EvalSpec (spec) where

import HeapBase.Eval
import HeapBase.CommonData
import Test.Hspec

spec :: Spec
spec = do
  describe "heapbase evaluate test" $ do
    it "7" $ do
      (evaluate "7") `shouldBe` (Number 7)
    it "(quote hello)" $ do
      (evaluate "(quote hello)") `shouldBe` (Atom "hello")
    it "((lambda (x y) y) 6 7))" $ do
      (evaluate "((lambda (x y) y) 6 7)") `shouldBe` (Number 7)
    it "(if #t 7 0)" $ do
      (evaluate "(if #t 7 0)") `shouldBe` (Number 7)
    it "((lambda (t) ((lambda (x) t) (set! t 7))) 0)" $ do
      (evaluate "((lambda (t) ((lambda (x) t) (set! t 7))) 0)") `shouldBe` (Number 7)
    it "(call/cc (lambda (c) (0 3 (c 7))))" $ do
      (evaluate "(call/cc (lambda (c) (0 3 (c 7))))") `shouldBe` (Number 7)
    it "((lambda (f x) (f x)) (lambda (x) x) 7)" $ do
      (evaluate "((lambda (f x) (f x)) (lambda (x) x) 7)") `shouldBe` (Number 7)
