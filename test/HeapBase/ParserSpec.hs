module HeapBase.ParserSpec (spec) where

import HeapBase.Parser
import HeapBase.CommonData
import Test.Hspec

spec :: Spec
spec = do
  describe "heap based parser test" $ do
    it "test 1" $ do
      myParse "(+ 1 2 3)" `shouldBe`(Right $ List [Atom "+",Number 1,Number 2,Number 3])
    it "(lambda (x y) y)" $ do
      myParse "(lambda (x y) y)" `shouldBe` (Right $ List [Atom "lambda", List [Atom "x", Atom "y"], Atom "y"])
