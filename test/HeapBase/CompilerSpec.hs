module HeapBase.CompilerSpec (spec) where

import HeapBase.Compiler
import HeapBase.Parser
import HeapBase.VM
import HeapBase.CommonData
import Test.Hspec

runCompile :: String -> VMInsn
runCompile s = case myParse s of
                 Right expr -> compile expr emptyCTEnv Halt
                 Left _ -> error "runCompile: parse failed."

runCompileTail :: String -> VMInsn
runCompileTail s = case myParse s of
                     Right expr -> compile expr emptyCTEnv Return
                     Left _ -> error "runCompile: parse failed."

-- from https://github.com/propella/3imp-westwood/blob/master/c34-heap-test.scm
-- just a port from Scheme to Haskell
spec :: Spec
spec = do
  describe "heapbase compiler test" $ do
    it "(quote hello)" $ do
      (runCompile "(quote hello)") `shouldBe` (Constant (Atom "hello") Halt)
    it "(lambda (c) c)" $ do
      (runCompile "(lambda (c) c)") `shouldBe` (Close (Refer 0 Return) Halt)
    it "(if #t 3 4)" $ do
      (runCompile "(if #t 3 4)") `shouldBe` (Constant
                                              (Bool True)
                                              (Test
                                               (Constant (Number 3) Halt)
                                               (Constant (Number 4) Halt)))
    it "(call/cc (lambda (c) c))" $ do
      (runCompile "(call/cc (lambda (c) c))") `shouldBe` (Frame
                                                           Halt
                                                           (Conti
                                                             (Argument
                                                               (Close
                                                                 (Refer 0 Return)
                                                                 Apply))))
    it "(call/cc (lambda (c) c)) at tail" $ do
      (runCompileTail "(call/cc (lambda (c) c))") `shouldBe` (Conti
                                                              (Argument
                                                               (Close
                                                                 (Refer 0 Return)
                                                                 Apply)))
    it "(call/cc (lambda (c) (0 (c 1))))" $ do
      (runCompileTail "(call/cc (lambda (c) (0 (c 1))))") `shouldBe` (Conti
                                                                       (Argument
                                                                         (Close
                                                                           (Frame
                                                                             (Argument
                                                                              (Constant
                                                                               (Number 0)
                                                                                (Apply)))
                                                                             (Constant
                                                                               (Number 1)
                                                                               (Argument
                                                                                (Refer
                                                                                 0
                                                                                 Apply))))
                                                                           Apply)))
    it "7" $ do
      (runCompile "7") `shouldBe` (Constant (Number 7) Halt)
